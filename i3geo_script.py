import os
import platform
import requests
import json
from shp2sql import *


def getCookie():
    request_cookie = '__utma=112314136.726863382.1584469734.1584469734.1584469734.1; __utmc=112314136; __utmz=112314136.1584469734.1.1.utmcsr=google|utmccn=(organic)|utmcmd=organic|utmctr=(not%20provided); SERVERID=geo04; BIGipServerpool_mapas=4026597642.20480.0000; i3geolingua=pt; i3GeoPHP=brs54g71v8k6bcc5ihk7f96217; TS01ad0d01='
    response_cookie_name = 'TS01ad0d01'

    try:
        r = requests.get(url_site)
    except:
        print("Erro na conexao")

    response_cookie_value = r.cookies[response_cookie_name]
    final_cookie = request_cookie + response_cookie_value

    return final_cookie


def downloadFiles(files, tema):    
    url_base = 'http://mapas.mma.gov.br/'
    dir_base = os.getcwd() + '/downloads/i3geo/'

    if (not os.path.exists(dir_base)):
        os.mkdir(dir_base)

    dir_path = dir_base + tema + '/'
    try:
        uri = files.split(',')
        for i in range(0, len(uri)):
            url = url_base + uri[i]
            r = requests.get(url)
            if (not os.path.exists(dir_path)):
                os.mkdir(dir_path)
            file_path = dir_path + uri[i].split('/')[1]
            with open(file_path, 'wb') as f:
                f.write(r.content)
    except:
        print("Erro")
        pass
    shapeToSQL(dir_path, '')


def getDownloadList(tema):
    url_downloads = 'http://mapas.mma.gov.br/i3geo/classesphp/mapa_controle.php?map_file=&funcao=download3&tema='+tema+'&cpaint_function=downloadTema2&cpaint_response_type=JSON'
    _cookies = {"Cookies": getCookie()}

    p = requests.post(url_downloads)

    json_data = json.loads(p.text)
    #json_data = json.loads(p.decode("utf-8"))

    downloadFiles(json_data['data']['arquivos'], tema)


def getListaTemasJSON(grupo, subgrupo):
    _form_data = {'cpaint_function': 'pegalistadetemas', 'cpaint_argument[]': '"funcao=pegalistadetemas&g_sid=&idmenu=18&grupo='+str(grupo)+'&subgrupo='+str(subgrupo)+'&map_file=&idioma=pt"', 'cpaint_response_type': 'JSON'}
    _cookies = {"Cookies": getCookie()}

    p = requests.post(url_list, data = _form_data, cookies = _cookies)

    json_data = json.loads(p.text)

    for tema in json_data['data']['temas']:
        if(tema['download'] == 'sim'):
            getDownloadList(tema['tid'])
    #return json_data


def getListaSubgruposJSON(grupo_id):
    _form_data = {'cpaint_function': 'pegalistadeSubgrupos', 'cpaint_argument[]': '"funcao=pegalistadeSubgrupos&g_sid=&idmenu=18&grupo='+str(grupo_id)+'&map_file=&idioma=pt"', 'cpaint_response_type': 'JSON'}
    _cookies = {"Cookies": getCookie()}

    p = requests.post(url_list, data = _form_data, cookies = _cookies)

    json_data = json.loads(p.text)

    for subgrupo in json_data['data']['subgrupo']:
        if(subgrupo['download'] == 'sim'):
            getListaTemasJSON(grupo_id, subgrupo['id_n2'])
        

def getListaGruposJSON():
    _form_data = {'cpaint_function': 'pegalistadegrupos', 'cpaint_argument[]': '"funcao=pegalistadegrupos&map_file=&g_sid=&idmenu=18&listasistemas=nao&listasgrupos=sim&idioma=pt"', 'cpaint_response_type': 'JSON'}
    _cookies = {"Cookies": getCookie()}

    p = requests.post(url_list, data = _form_data, cookies = _cookies)

    json_data = json.loads(p.text)

    
    for grupo in json_data['data']['grupos']:
        if('nome' in grupo):
            getListaSubgruposJSON(grupo['id_n1'])



def getListaMenuJSON():
    _form_data = {'cpaint_function': 'pegalistademenus', 'cpaint_argument[]': '"funcao=pegalistademenus&g_sid=&map_file=&idioma=pt"', 'cpaint_response_type': 'JSON'}
    _cookies = {"Cookies": getCookie()}

    p = requests.post(url_list, data = _form_data, cookies = _cookies)

    json_data = json.loads(p.text)

    getListaGruposJSON()
    #return json_data['data'][0]['nomemenu']

getListaMenuJSON()
