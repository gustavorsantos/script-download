# Shapefile Downloader Script

Esse repositório reúne uma série de scripts com o intuito de baixar arquivos geográficos de diversas fontes diferentes.

Como cada site tem um comportamento diferente há a necessidade da criação de um script por fonte de dados, porém há funções que servem mais de um script, como é o caso da *shapeToSQL()*.

No momento há duas bases de dados:
* i3Geo
* Funai

## Função shapeToSQL()

Essa função consegue ser usada em basicamente todos os scripts, uma vez que ela é a responsável pela conversão de .shp para .sql e pelo carregamento dos arquivos .sql para o banco de dados PostgreSQL.

## Configuração do Banco

Instale o postgresql e o postgis

``` 
sudo apt-get install postgresql && sudo apt-get install postgis
```
OBS: Recomendado não alterar a porta padrão (5432).

Acesse o Postgre com

```
sudo -u postgres psql
```

Crie o banco de dados

```
CREATE DATABASE shapefiles_db;
```

Crie um usuário
```
CREATE USER shp_user;
```
Dê os privilégios do banco para ele 
```
GRANT ALL ON DATABASE shapefiles_db TO shp_user;
```
Acesse o banco 
```
\c shapefiles_db
```

Crie uma extensão para o PostGis
```
CREATE EXTENSION postgis
```

Altere as configurações de autenticação no arquivo pg_hba.conf.

Ache onde está seu diretório, ainda no console do psql:
```
SHOW hba_file;
```
Copie o resultado e digite
```
\q
```
```
sudo nano caminho/pg_hba.conf
```
Adicione a linha

```
local   all             shp_user        127.0.0.1               trust
```

