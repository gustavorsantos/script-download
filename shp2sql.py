import os
import subprocess

if (not os.path.exists(os.getcwd() + '/downloads/')):
        os.mkdir(os.getcwd() + '/downloads/')

def importToDatabase(path, dir_name):
    file_sql = ''
    for file in os.listdir(os.path.dirname(path)+'/'+dir_name):
        if file.endswith(".sql"):
            file_sql = file
            break
    psql = [
        'psql',
        '-U',
        'shp_user',
        '-h',
        'localhost',
        '-d',
        'shapefiles_db',
        '-p',
        '5432',
        '-f',
        os.path.dirname(path)+'/'+dir_name +'/' + file_sql
    ]

    subprocess.call(psql, shell=False)


def shapeToSQL(path, dir_name):
    file_shp = ''
    for file in os.listdir(os.path.dirname(path)+'/'+dir_name):
        if file.endswith(".shp"):
            file_shp = file
            break

    file_shp = file_shp.split('.')[0]

    input_file = os.path.dirname(path) + '/' + dir_name + '/' + file_shp + '.shp'
    output_file = os.path.dirname(path) + '/' + dir_name + '/' + file_shp + '.sql'
    
    shp2pgsql = [
        'shp2pgsql',
        '-d',
        '-W',
        '"latin1"',
        input_file,
        '>',
        output_file
    ]
    
    try:
        subprocess.call(shp2pgsql, shell=False)
        importToDatabase(path, dir_name)
        print("Success!")
    except:
        print("Conversion shapefile to sql failed!")


    
