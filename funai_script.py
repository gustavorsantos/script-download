import requests
import os
import datetime
import subprocess
from shp2sql import *
from zipfile import ZipFile 
from xml.etree import ElementTree as ET

#shp2pgsql states.shp statestable teste_db > states.sql
#sudo chown -R $USER:$USER /opt/teste/

dt = datetime.datetime.today()
dateString = str(dt.year) + '_' + str(dt.month) + '_'+ str(dt.day)

if (not os.path.exists(os.getcwd() + '/downloads/')):
        os.mkdir(os.getcwd() + '/downloads/')

def unzip(path, dir_name):
    with ZipFile(path, 'r') as zip:
        zip.extractall(os.path.dirname(path) + '/' + dir_name)
        os.remove(path)
        print("\tExtraido com sucesso!")


def downloadFile(filename):
    dir_base = os.getcwd() + '/downloads/funai/'
    dir = dir_base + dateString

    if (not os.path.exists(dir_base)):
        os.mkdir(dir_base)

    if (not os.path.exists(dir)):
        os.mkdir(dir)

    url_down = 'http://geoserver.funai.gov.br/geoserver/Funai/ows?service=WFS&version=1.0.0&request=GetFeature&typeName='+filename+'&outputFormat=SHAPE-ZIP'
    r = requests.get(url_down)

    filename = filename.replace(':', '_')
    file_path = dir + '/' + filename +'.zip'

    with open(file_path, 'wb') as f:
        f.write(r.content)

    unzip(file_path, filename)
    shapeToSQL(file_path, filename)


def getXML():
    
    url_titles = 'http://geoserver.funai.gov.br/geoserver/Funai/ows?SERVICE=WFS&REQUEST=GetCapabilities'
    r = requests.get(url_titles)
    root = ET.fromstring(r.content)
    
    # retturns a collection of FeatureTyope Elements
    #try:
    for featurelist in root.findall('{http://www.opengis.net/wfs/2.0}FeatureTypeList'):
        for name in featurelist:
            name_value = name.find('{http://www.opengis.net/wfs/2.0}Name').text
            print("Arquivo a ser baixado: "+ name_value)
            downloadFile(name_value)
    print("\033[92mAll files downloaded successfully\x1b[0m")

getXML()
